package com.example.demo.product.util;


public class Constants {

    public static final class Keycloak {
        public static final String REALM_MASTER = "master";
        public static final String ADMIN_CLI_CLIENT_ID = "admin-cli";
    }
    
}
