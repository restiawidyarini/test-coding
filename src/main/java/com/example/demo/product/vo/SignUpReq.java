package com.example.demo.product.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import java.util.Date;


@Data
public class SignUpReq {

    private String username;
    private String email;

    @JsonProperty("encripted_password")
    private String password;
    private String phone;
    private String address;
    private String city;
    private String country;
    private String name;
    private String postcode;

}
