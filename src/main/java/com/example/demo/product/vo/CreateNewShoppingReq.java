package com.example.demo.product.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import java.util.Date;


@Data
public class CreateNewShoppingReq {

    String createdDate;

    String name;

}
