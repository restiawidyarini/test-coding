package com.example.demo.product.controller;

import com.example.demo.product.service.*;
import com.example.demo.product.vo.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api")

public class ShoppingController {

    @Autowired
    ShoppingService shoppingService;

    @RequestMapping(value = "/shopping",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> createShopping(@RequestBody CreateNewShoppingReq req) {
        
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            Object result = shoppingService.createShopping(req);

            @Override
            public HttpStatus processStatus() {
                return result != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
            }
            
            @Override
            public Object processRequest() {
                return result;
            }

            @Override
            public String processMessage() {
                return result != null ? "Success to create shopping" : "Failed to create shopping";
            }
        };
        return handler.getResult();
    }

    @RequestMapping(value = "/shopping/all",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getAllShopping() {
        
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            Object result = shoppingService.getAllShopping();

            @Override
            public HttpStatus processStatus() {
                return result != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
            }
            
            @Override
            public Object processRequest() {
                return result;
            }

            @Override
            public String processMessage() {
                return result != null ? "Success to get All shopping" : "Failed to get All Shopping";
            }
        };
        return handler.getResult();
    }


    @RequestMapping(value = "/shopping",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getShoppingById(@RequestParam(value = "id", required = false) String id) {
        
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            Object result = shoppingService.getShoppingById(id);

            @Override
            public HttpStatus processStatus() {
                return result != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
            }
            
            @Override
            public Object processRequest() {
                return result;
            }

            @Override
            public String processMessage() {
                return result != null ? "Success to get shopping" : "Failed to get Shopping";
            }
        };
        return handler.getResult();
    }


    @RequestMapping(value = "/shopping",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> updateShoppingById(@RequestBody CreateNewShoppingReq req,
                                                    @RequestParam(value = "id", required = false) String id) {
        
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            Object result = shoppingService.updateShopping(req, id);

            @Override
            public HttpStatus processStatus() {
                return result != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
            }
            
            @Override
            public Object processRequest() {
                return result;
            }

            @Override
            public String processMessage() {
                return result != null ? "Success to update shopping" : "Failed to update Shopping";
            }
        };
        return handler.getResult();
    }


    @RequestMapping(value = "/shopping",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> deleteShoppingById(@RequestParam(value = "id", required = false) String id) {
        
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            Object result = shoppingService.deleteShopping(id);

            @Override
            public HttpStatus processStatus() {
                return result != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
            }
            
            @Override
            public Object processRequest() {
                return result;
            }

            @Override
            public String processMessage() {
                return result != null ? "Success to delete shopping" : "Failed to delete Shopping";
            }
        };
        return handler.getResult();
    }


}