package com.example.demo.product.controller;

import org.springframework.http.HttpStatus;

import com.example.demo.product.exception.ProductException;
import com.example.demo.product.vo.ResultVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public abstract class AbstractRequestHandler {

    private static final Logger log = LoggerFactory.getLogger(AbstractRequestHandler.class);

    public ResponseEntity<ResultVO> getResult() {
        ResponseEntity<ResultVO> response = null;
        ResultVO result = new ResultVO();
        String msg = processMessage();
        HttpStatus status = processStatus();
        try {

            Object obj = processRequest();
            if (obj != null) {
                result.setStatus(status.value());
                result.setMessage(msg);
                result.setResults(obj);
                response = ResponseEntity.status(status).body(result);
            }else {
                result.setStatus(status.value());
                result.setMessage(msg);
                result.setResults(null);
                response = ResponseEntity.status(status).body(result);
            }
        } catch (ProductException e) {
            result.setStatus(e.getStatus().value());
            result.setMessage(msg);
            result.setResults(e.getMessage());

            log.error("ERROR", e);
        }
        return response;
    }


    public abstract Object processRequest();
    public abstract String processMessage();
    public abstract HttpStatus processStatus();
}
