package com.example.demo.product.exception;

import org.springframework.http.HttpStatus;

import com.example.demo.product.enums.*;
import lombok.Data;

@Data
public class ProductException extends RuntimeException {

    private Status code = null;
    private HttpStatus status = null;

    public ProductException() {
        super();
    }

    public ProductException(String message) {
        super(message);
    }

    public ProductException(String message, Status code) {
        super(message);
        this.code = code;
    }

    public ProductException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public ProductException(String message, HttpStatus status, Status code) {
        super(message);
        this.status = status;
        this.code = code;
    }
}
