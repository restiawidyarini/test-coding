package com.example.demo.product.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.product.enums.Status;
import com.example.demo.product.vo.ResultVO;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);
    
    @ExceptionHandler(value = {Exception.class, RuntimeException.class })
    public ResponseEntity<?> defaultErrorHandler(HttpServletRequest req, Exception e) {

        log.error(String.format("%s : Caught in Global Exception Handler for req: %s",
                e.getLocalizedMessage(), req.getRequestURL()));

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        ResultVO restResponseVO = new ResultVO();
        restResponseVO.setStatus(status.value());
        restResponseVO.setMessage(Status.ERROR.name());
        restResponseVO.setResults(e.getMessage());

        return new ResponseEntity<>(restResponseVO, status);
    }

    @ExceptionHandler(value = {ProductException.class })
    public ResponseEntity<?> defaultErrorHandler(HttpServletRequest req, ProductException e) {

        log.error(String.format("%s : Caught in Global Exception Handler for req: %s",
                e.getLocalizedMessage(), req.getRequestURL()));

        HttpStatus status;
        if(!StringUtils.isEmpty(e.getStatus())){
            status = e.getStatus();
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        Status statusCode;
        if(!StringUtils.isEmpty(e.getCode())){
            statusCode = e.getCode();
        } else {
            statusCode = Status.ERROR;
        }

        ResultVO restResponseVO = new ResultVO();
        restResponseVO.setResults(e.getMessage());
        restResponseVO.setMessage(statusCode.name());
        restResponseVO.setStatus(status.value());

        return new ResponseEntity<>(restResponseVO, status);
    }
}
