package com.example.demo.product.enums;

public enum Status{
    OK,
    ERROR,
    FAILED,
    UNAUTHORIZED;
    
}
