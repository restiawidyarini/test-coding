package com.example.demo.product.persistence.repository;

import java.util.List;

import com.example.demo.product.persistence.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface ShoppingRepository extends JpaRepository<Shopping, Integer>, JpaSpecificationExecutor<Shopping> {

    List<Shopping> findAll();
    Shopping findByUuid(String id);

}
