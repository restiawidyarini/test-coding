package com.example.demo.product.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.product.persistence.domain.*;
import com.example.demo.product.persistence.repository.*;
import com.example.demo.product.vo.*;

import java.text.SimpleDateFormat;  
import java.util.Date;
import java.text.ParseException;


@Service
public class ShoppingService {

    private static final Logger log = LoggerFactory.getLogger(ShoppingService.class);

    @Autowired
    ShoppingRepository shoppingRepository;

    @Transactional
    public Shopping createShopping(CreateNewShoppingReq req) {

        log.info("shopping : "+req);

        Shopping shopping = new Shopping();
        Date date = parseDate(req.getCreatedDate(), "dd-MM-yyyy");  

        shopping.setCreationDate(date);
        shopping.setName(req.getName());
        shoppingRepository.saveAndFlush(shopping);
        log.info("shopping : "+shopping);

        return shopping;

    }

    @Transactional
    public List<Shopping> getAllShopping() {

        //check database
        List<Shopping> shopping = (List<Shopping>) shoppingRepository.findAll();
        log.info("List shopping :"+shopping);

        return shopping;

    }

    @Transactional
    public Shopping getShoppingById(String id) {

        //check database
        Shopping shopping = shoppingRepository.findByUuid(id);
        log.info("List shopping By Id :"+shopping);

        return shopping;

    }

    @Transactional
    public Shopping updateShopping(CreateNewShoppingReq req, String id) {

        log.info("shopping : "+req);

        //check database
        Shopping shopping = shoppingRepository.findByUuid(id);
        log.info("List shopping By Id :"+shopping);

        Date date = parseDate(req.getCreatedDate(), "yyyy-MM-dd");  

        shopping.setCreationDate(date);
        shopping.setName(req.getName());
        shoppingRepository.saveAndFlush(shopping);
        log.info("shopping : "+shopping);

        return shopping;

    }

    @Transactional
    public Shopping deleteShopping(String id) {

        //check database
        Shopping shopping = shoppingRepository.findByUuid(id);
        log.info("List shopping By Id :"+shopping);

        shoppingRepository.delete(shopping);

        return shopping;

    }


    public static Date parseDate(String dateString, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            return simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            log.error("error parse date");
            e.printStackTrace();
        }
        return null;
    }

    
}
